package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/patrickmn/go-cache"
	"github.com/tedkimdev/go-backend-h-b/internal/api/handlers"
	"github.com/tedkimdev/go-backend-h-b/internal/api/handlers/postgrp"
	"github.com/tedkimdev/go-backend-h-b/internal/api/middleware"
	"github.com/tedkimdev/go-backend-h-b/internal/domain/posts"
	"github.com/tedkimdev/go-backend-h-b/internal/repository"
	"go.uber.org/zap"
)

type Config struct {
	Log   *zap.SugaredLogger
	Cache *cache.Cache
}

func NewAPIServer(cfg *Config) *http.Server {
	r := gin.Default()
	r.Use(middleware.ErrorHandler(cfg.Log))

	rg := r.Group("/api")

	rg.Handle("GET", "/ping", handlers.Ping)

	pr := repository.NewPostRepository(cfg.Cache, cfg.Log)
	ps := posts.NewService(cfg.Log, pr)
	ph := postgrp.NewHandler(ps)
	rg.Handle("GET", "/posts", ph.GetPosts)

	srv := http.Server{
		Addr:    ":8080",
		Handler: r,
	}
	return &srv
}
