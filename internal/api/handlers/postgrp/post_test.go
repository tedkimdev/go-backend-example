package postgrp_test

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/tedkimdev/go-backend-h-b/internal/api/handlers/postgrp"
	"github.com/tedkimdev/go-backend-h-b/internal/domain/posts"
	"github.com/tedkimdev/go-backend-h-b/internal/models"
	"github.com/tedkimdev/go-backend-h-b/internal/utils"
)

func TestGetPosts(t *testing.T) {
	testCases := map[string]struct {
		ps       posts.Service
		queries  map[string]string
		extError string
		extCode  int
	}{
		"fail on tag query": {
			ps:       &MockPostService{},
			queries:  nil,
			extError: postgrp.ErrTagQuery.Error(),
			extCode:  http.StatusBadRequest,
		},
		"fail on invalid sortBy": {
			ps: &MockPostService{},
			queries: map[string]string{
				"tags":   "tag",
				"sortBy": "invalid",
			},
			extError: postgrp.ErrSortQuery.Error(),
			extCode:  http.StatusBadRequest,
		},
		"fail on invalid direction": {
			ps: &MockPostService{},
			queries: map[string]string{
				"tags":      "tag",
				"direction": "invalid",
			},
			extError: postgrp.ErrSortQuery.Error(),
			extCode:  http.StatusBadRequest,
		},
		"fail on invalid sortBy and direction": {
			ps: &MockPostService{},
			queries: map[string]string{
				"tags":      "tag",
				"sortBy":    "invalid",
				"direction": "invalid",
			},
			extError: postgrp.ErrSortQuery.Error(),
			extCode:  http.StatusBadRequest,
		},
		"fail on GetPostsByTags": {
			ps: &MockPostService{srvErr: true},
			queries: map[string]string{
				"tags":      "tech",
				"sortBy":    "id",
				"direction": "asc",
			},
			extError: "getting posts: service error",
			extCode:  http.StatusInternalServerError,
		},
		"success": {
			ps: &MockPostService{success: true},
			queries: map[string]string{
				"tags":      "tech",
				"sortBy":    "id",
				"direction": "asc",
			},
			extError: "",
			extCode:  http.StatusOK,
		},
	}

	for key, tt := range testCases {
		t.Run(key, func(t *testing.T) {
			// arrange
			w := httptest.NewRecorder()
			ctx := utils.GinTestContext(w)
			if tt.queries != nil {
				utils.SetTestQueryParams(ctx, tt.queries)
			}
			ph := postgrp.NewHandler(tt.ps)

			// action
			ph.GetPosts(ctx)

			// assert
			assert.Equal(t, w.Code, tt.extCode)
			if key != "success" {
				assert.GreaterOrEqual(t, len(ctx.Errors), 1)
				assert.Equal(t, ctx.Errors[0].Error(), tt.extError)
				assert.True(t, ctx.IsAborted())
			} else {
				var data struct {
					Posts []models.Post `json:"posts"`
				}
				if err := json.NewDecoder(w.Body).Decode(&data); err != nil {
					require.NoError(t, err)
				}
				assert.Equal(t, len(data.Posts), 2)
			}
		})
	}
}

type MockPostService struct {
	success bool
	srvErr  bool
}

func (s *MockPostService) GetPostsByTags(ctx context.Context, tags []string, sortBy, direction string) ([]models.Post, error) {
	if s.srvErr {
		return nil, errors.New("service error")
	}
	if s.success {
		posts := []models.Post{}
		posts = append(posts, models.Post{})
		posts = append(posts, models.Post{})
		return posts, nil
	}
	return nil, nil
}
