package postgrp

import (
	"errors"
	"fmt"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/tedkimdev/go-backend-h-b/internal/domain/posts"
)

type Handler struct {
	ps posts.Service
}

var (
	ErrTagQuery  = errors.New("Tags parameter is required")
	ErrSortQuery = errors.New("sortBy parameter is invalid")
)

func NewHandler(postService posts.Service) *Handler {
	return &Handler{ps: postService}
}

func (h *Handler) GetPosts(ctx *gin.Context) {
	tagsQuery := ctx.Query("tags")
	sortBy := "id"
	direction := "asc"

	if sortByQuery := ctx.Query("sortBy"); sortByQuery != "" {
		sortBy = sortByQuery
	}
	if directionQuery := ctx.Query("direction"); directionQuery != "" {
		direction = directionQuery
	}

	if tagsQuery == "" {
		ctx.AbortWithError(http.StatusBadRequest, ErrTagQuery)
		return
	}
	if !validateSortQuery(sortBy, direction) {
		ctx.AbortWithError(http.StatusBadRequest, ErrSortQuery)
		return
	}

	tags := strings.Split(tagsQuery, ",")

	posts, err := h.ps.GetPostsByTags(ctx, tags, sortBy, direction)
	if err != nil {
		ctx.AbortWithError(http.StatusInternalServerError, fmt.Errorf("getting posts: %w", err))
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"posts": posts,
	})
}

func validateSortQuery(sortBy, direction string) bool {
	isValid := true

	switch sortBy {
	case "id", "reads", "likes", "popularity":
	default:
		isValid = false
	}
	if !isValid {
		return isValid
	}

	switch direction {
	case "desc", "asc":
	default:
		isValid = false
	}
	return isValid
}
