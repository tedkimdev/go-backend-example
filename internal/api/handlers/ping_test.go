package handlers_test

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/tedkimdev/go-backend-h-b/internal/api/handlers"
	"github.com/tedkimdev/go-backend-h-b/internal/utils"
)

func TestPing(t *testing.T) {
	w := httptest.NewRecorder()
	ctx := utils.GinTestContext(w)

	handlers.Ping(ctx)

	type response struct {
		Success bool `json:"success"`
	}
	var resp response
	if err := json.NewDecoder(w.Body).Decode(&resp); err != nil {
		require.NoError(t, err)
	}

	assert.EqualValues(t, http.StatusOK, w.Code)
	assert.EqualValues(t, resp.Success, true)
}
