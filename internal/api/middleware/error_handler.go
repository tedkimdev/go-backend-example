package middleware

import (
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

func ErrorHandler(log *zap.SugaredLogger) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ctx.Next()

		if len(ctx.Errors) > 0 {
			for _, ginErr := range ctx.Errors {
				log.Errorw("ERROR", "message", ginErr)
			}
			ctx.JSON(-1, gin.H{
				"error": ctx.Errors[len(ctx.Errors)-1],
			})
		}
	}
}
