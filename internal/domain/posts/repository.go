package posts

import (
	"context"

	"github.com/tedkimdev/go-backend-h-b/internal/models"
)

type Repository interface {
	GetPostsByTag(ctx context.Context, tag string) ([]models.Post, error)
	SetPostsBy(ctx context.Context, tag string, post []models.Post) error
}
