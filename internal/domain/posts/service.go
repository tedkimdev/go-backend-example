package posts

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"sort"
	"sync"

	"github.com/tedkimdev/go-backend-h-b/internal/models"
	"go.uber.org/zap"
)

// TODO: move to configuration file
const (
	HatchwaysAPI = "https://api.hatchways.io/assessment/blog/posts"
)

var (
	ErrNotFound = errors.New("posts not found")
)

type PostQuery struct {
	Tag       string
	SortBy    string
	Direction string
}

type PostResult struct {
	Tag   string
	Posts []models.Post
	Error error
}

type Service interface {
	GetPostsByTags(ctx context.Context, tags []string, sortBy, direction string) ([]models.Post, error)
}

type service struct {
	log  *zap.SugaredLogger
	repo Repository
}

func NewService(log *zap.SugaredLogger, r Repository) *service {
	return &service{
		log:  log,
		repo: r,
	}
}

func (s *service) GetPostsByTags(ctx context.Context, tags []string, sortBy, direction string) ([]models.Post, error) {
	resultChan := make(chan *PostResult)
	postsMap := make(map[string][]models.Post)

	var wg sync.WaitGroup
	for _, tag := range tags {
		wg.Add(1)
		go s.getPostsByTag(ctx, tag, resultChan, &wg)
	}
	go func() {
		wg.Wait()
		close(resultChan)
	}()

	for result := range resultChan {
		postsMap[result.Tag] = result.Posts
	}

	posts := []models.Post{}

	// merge all and remove duplicate
	keyMap := make(map[int]bool)
	for _, ps := range postsMap {
		for _, p := range ps {
			if _, ok := keyMap[p.ID]; !ok {
				keyMap[p.ID] = true
				posts = append(posts, p)
			}
		}
	}

	if len(posts) == 0 {
		return nil, ErrNotFound
	}

	if err := s.sortPosts(posts, sortBy, direction); err != nil {
		return nil, fmt.Errorf("sorting posts: %w", err)
	}

	return posts, nil
}

func (s *service) getPostsByTag(ctx context.Context, tag string, resultChan chan *PostResult, wg *sync.WaitGroup) {
	defer wg.Done()
	queryString := fmt.Sprintf("?tag=%s", tag)
	url := HatchwaysAPI + queryString

	// get posts from cache first
	posts, err := s.repo.GetPostsByTag(ctx, tag)
	if err != nil {
		s.log.Errorf("getting posts from cache: %w", err)
	}
	if len(posts) > 0 {
		s.log.Infow("cache hit", "tag", tag, "posts length", len(posts))
		resultChan <- &PostResult{
			Tag:   tag,
			Posts: posts,
			Error: nil,
		}
		return
	}

	s.log.Infow("API Call", "URL", url)
	resp, err := http.Get(url)
	if err != nil {
		resultChan <- &PostResult{
			Posts: nil,
			Error: fmt.Errorf("getting post by tag[%s]: %w", tag, err),
		}
		return
	}

	type response struct {
		Posts []models.Post `json:"posts"`
	}
	var data response

	if err := json.NewDecoder(resp.Body).Decode(&data); err != nil {
		resultChan <- &PostResult{
			Posts: nil,
			Error: fmt.Errorf("decoding json tag[%s]: %w", tag, err),
		}
		return
	}

	// set posts in cache
	s.log.Infow("set cache", "tag", tag, "posts length", len(data.Posts))
	if err := s.repo.SetPostsBy(ctx, tag, data.Posts); err != nil {
		s.log.Errorf("getting posts from cache: %w", err)
	}

	resultChan <- &PostResult{
		Tag:   tag,
		Posts: data.Posts,
		Error: nil,
	}
}

func (s *service) sortPosts(posts []models.Post, sortBy, direction string) error {
	s.log.Infow("sorting posts", "sortBy", sortBy, "direction", direction)
	switch sortBy {
	case "id":
		if direction == "asc" {
			sort.Sort(models.ByIDASC(posts))
			break
		}
		sort.Sort(models.ByIDDESC(posts))
	case "reads":
		if direction == "asc" {
			sort.Sort(models.ByReadsASC(posts))
			break
		}
		sort.Sort(models.ByReadsDESC(posts))
	case "likes":
		if direction == "asc" {
			sort.Sort(models.ByLikesASC(posts))
			break
		}
		sort.Sort(models.ByLikesDESC(posts))
	case "popularity":
		if direction == "asc" {
			sort.Sort(models.ByPopularityASC(posts))
			break
		}
		sort.Sort(models.ByPopularityDESC(posts))
	default:
		return fmt.Errorf("sorting sortBy[%s]", sortBy)
	}
	return nil
}
