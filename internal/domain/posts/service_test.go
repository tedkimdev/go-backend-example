package posts

import (
	"context"
	"fmt"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/tedkimdev/go-backend-h-b/internal/models"
	"github.com/tedkimdev/go-backend-h-b/pkg/logger"
)

func TestGetPostsByTags(t *testing.T) {
	testCases := map[string]struct {
		tags      []string
		sortBy    string
		direction string
		expErr    string
		expPost   []models.Post
	}{
		"not found": {
			tags:      []string{"no"},
			sortBy:    "id",
			direction: "asc",
			expErr:    "posts not found",
		},
		"invalid sorting": {
			tags:      []string{"tech"},
			sortBy:    "invalid",
			direction: "invalid",
			expErr:    "sorting posts: ",
		},
		"success": {
			sortBy:    "id",
			direction: "asc",
			expErr:    "",
		},
	}

	for key, tt := range testCases {
		t.Run(key, func(t *testing.T) {
			ctx := context.Background()
			log, err := logger.New("POST SERVICE")

			if err != nil {
				require.NoError(t, err)
			}

			mockRepo := MockPostsRepo{}

			srv := NewService(log, &mockRepo)
			posts, err := srv.GetPostsByTags(ctx, tt.tags, tt.sortBy, tt.direction)

			if err != nil {
				assert.True(t, strings.Contains(err.Error(), tt.expErr))
			} else {
				assert.True(t, len(posts) > 0)
			}
		})
	}
}

func TestGetPostsByTag(t *testing.T) {
	// TODO: not implemented.
}

func TestSortPosts(t *testing.T) {
	posts := []models.Post{
		{
			ID:         4,
			LikeCount:  10,
			ReadCount:  99,
			Popularity: 55.5,
		},
		{
			ID:         5,
			LikeCount:  20,
			ReadCount:  199,
			Popularity: 9.9,
		},
		{
			ID:         6,
			LikeCount:  30,
			ReadCount:  299,
			Popularity: 10.0,
		},
	}
	testCases := map[string]struct {
		posts     []models.Post
		sortBy    string
		direction string
		expInt    int
		expFloat  float64
		expErr    string
	}{
		"error": {
			posts:     nil,
			sortBy:    "error",
			direction: "error",
			expErr:    fmt.Sprintf("sorting sortBy[%s]", "error"),
		},
		"id asc": {
			posts: append(posts, models.Post{
				ID: 1,
			}),
			sortBy:    "id",
			direction: "asc",
			expInt:    1,
		},
		"id desc": {
			posts: append(posts, models.Post{
				ID: 999,
			}),
			sortBy:    "id",
			direction: "desc",
			expInt:    999,
		},
		"likes asc": {
			posts: append(posts, models.Post{
				LikeCount: 0,
			}),
			sortBy:    "likes",
			direction: "asc",
			expInt:    0,
		},
		"likes desc": {
			posts: append(posts, models.Post{
				LikeCount: 999,
			}),
			sortBy:    "likes",
			direction: "desc",
			expInt:    999,
		},
		"reads asc": {
			posts: append(posts, models.Post{
				ReadCount: 0,
			}),
			sortBy:    "reads",
			direction: "asc",
			expInt:    0,
		},
		"reads desc": {
			posts: append(posts, models.Post{
				ReadCount: 10000,
			}),
			sortBy:    "reads",
			direction: "desc",
			expInt:    10000,
		},
		"popularity asc": {
			posts: append(posts, models.Post{
				Popularity: 1.1,
			}),
			sortBy:    "popularity",
			direction: "asc",
			expFloat:  1.1,
		},
		"popularity desc": {
			posts: append(posts, models.Post{
				Popularity: 99.9,
			}),
			sortBy:    "popularity",
			direction: "desc",
			expFloat:  99.9,
		},
	}

	for key, tt := range testCases {
		t.Run(key, func(t *testing.T) {
			log, err := logger.New("POST SERVICE")
			if err != nil {
				require.NoError(t, err)
			}
			mockRepo := MockPostsRepo{}

			srv := NewService(log, &mockRepo)
			err = srv.sortPosts(tt.posts, tt.sortBy, tt.direction)

			if err != nil {
				assert.Equal(t, err.Error(), tt.expErr)
			} else {
				if strings.Contains(key, "popularity") {
					assert.Equal(t, tt.posts[0].Popularity, tt.expFloat)
				}
				if strings.Contains(key, "id") {
					assert.Equal(t, tt.posts[0].ID, tt.expInt)
				}
				if strings.Contains(key, "reads") {
					assert.Equal(t, tt.posts[0].ReadCount, tt.expInt)
				}
				if strings.Contains(key, "likes") {
					assert.Equal(t, tt.posts[0].LikeCount, tt.expInt)
				}
			}
		})
	}
}

type MockPostsRepo struct {
}

func (r *MockPostsRepo) GetPostsByTag(ctx context.Context, tag string) ([]models.Post, error) {
	return nil, nil
}
func (r *MockPostsRepo) SetPostsBy(ctx context.Context, tag string, post []models.Post) error {
	return nil
}
