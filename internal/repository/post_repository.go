package repository

import (
	"context"
	"errors"

	"github.com/patrickmn/go-cache"
	"github.com/tedkimdev/go-backend-h-b/internal/models"
	"go.uber.org/zap"
)

type MemoryPostRepository struct {
	c   *cache.Cache
	log *zap.SugaredLogger
}

func NewPostRepository(c *cache.Cache, log *zap.SugaredLogger) *MemoryPostRepository {
	return &MemoryPostRepository{
		c:   c,
		log: log,
	}
}

func (r *MemoryPostRepository) GetPostsByTag(ctx context.Context, tag string) ([]models.Post, error) {
	values, found := r.c.Get(tag)
	if !found {
		return nil, nil
	}
	posts, ok := values.([]models.Post)
	if !ok {
		return nil, errors.New("failed to type assertion")
	}
	return posts, nil
}

func (r *MemoryPostRepository) SetPostsBy(ctx context.Context, tag string, posts []models.Post) error {
	r.c.Set(tag, posts, cache.DefaultExpiration)
	return nil
}
