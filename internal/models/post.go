package models

type Post struct {
	ID         int      `json:"id"`
	Tags       []string `json:"tags"`
	LikeCount  int      `json:"likes"`
	ReadCount  int      `json:"reads"`
	Popularity float64  `json:"popularity"`
	AuthorID   int      `json:"authorId"`
	Author     string   `json:"author"`
}

type ByIDASC []Post

func (p ByIDASC) Len() int           { return len(p) }
func (p ByIDASC) Less(i, j int) bool { return p[i].ID < p[j].ID }
func (p ByIDASC) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

type ByIDDESC []Post

func (p ByIDDESC) Len() int           { return len(p) }
func (p ByIDDESC) Less(i, j int) bool { return p[i].ID > p[j].ID }
func (p ByIDDESC) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

type ByReadsASC []Post

func (p ByReadsASC) Len() int           { return len(p) }
func (p ByReadsASC) Less(i, j int) bool { return p[i].ReadCount < p[j].ReadCount }
func (p ByReadsASC) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

type ByReadsDESC []Post

func (p ByReadsDESC) Len() int           { return len(p) }
func (p ByReadsDESC) Less(i, j int) bool { return p[i].ReadCount > p[j].ReadCount }
func (p ByReadsDESC) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

type ByLikesASC []Post

func (p ByLikesASC) Len() int           { return len(p) }
func (p ByLikesASC) Less(i, j int) bool { return p[i].LikeCount < p[j].LikeCount }
func (p ByLikesASC) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

type ByLikesDESC []Post

func (p ByLikesDESC) Len() int           { return len(p) }
func (p ByLikesDESC) Less(i, j int) bool { return p[i].LikeCount > p[j].LikeCount }
func (p ByLikesDESC) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

type ByPopularityASC []Post

func (p ByPopularityASC) Len() int           { return len(p) }
func (p ByPopularityASC) Less(i, j int) bool { return p[i].Popularity < p[j].Popularity }
func (p ByPopularityASC) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

type ByPopularityDESC []Post

func (p ByPopularityDESC) Len() int           { return len(p) }
func (p ByPopularityDESC) Less(i, j int) bool { return p[i].Popularity > p[j].Popularity }
func (p ByPopularityDESC) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }
