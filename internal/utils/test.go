package utils

import (
	"net/http"
	"net/http/httptest"
	"net/url"

	"github.com/gin-gonic/gin"
)

func GinTestContext(w *httptest.ResponseRecorder) *gin.Context {
	gin.SetMode(gin.TestMode)
	ctx, _ := gin.CreateTestContext(w)
	ctx.Request = &http.Request{
		Header: make(http.Header),
		URL:    &url.URL{},
	}

	return ctx
}

func SetTestQueryParams(ctx *gin.Context, queryMap map[string]string) {
	u := url.Values{}
	for queryKey, v := range queryMap {
		u.Add(queryKey, v)
	}
	ctx.Request.URL.RawQuery = u.Encode()
}
