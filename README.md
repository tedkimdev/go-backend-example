# Go-Backend

## How to run server

```
  make tidy
  make dev
```

## How to run test
```
make test

OR 

make test-cover
```

### Used Libaries
- gin : rest api
- zap : logger
- go-cache : momery cache
- testify : test

### Structrues

```
.
├── cmd
│   └── post-api
│       └── main.go
├── docker
├── internal
│   ├── api
│   │   ├── handlers
│   │   │   └── postgrp        // post handler
│   │   ├── middleware         // middleware
│   │   └── server.go          // initialize server
│   ├── domain
│   │   └── posts              // post logic
│   ├── models
│   │   └── post.go
│   ├── repository
│   │   └── post_repository.go // post cache repository
│   └── utils
│       └── test.go            // test util functions
└── pkg
    └── logger
        └── logger.go
```


### TODO
- improve query binding in handlers 
- add configuration(viper)
- add repository test
- complete post service test
- mock zap logger in test
- etc