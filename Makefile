dev:
	go run cmd/post-api/main.go

test:
	go test ./... -v

test-cover:
	go test ./... -cover

tidy:
	go mod tidy
	go mod vendor