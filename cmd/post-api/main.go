package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/patrickmn/go-cache"
	"github.com/tedkimdev/go-backend-h-b/internal/api"
	"github.com/tedkimdev/go-backend-h-b/pkg/logger"
	"go.uber.org/zap"
)

func main() {
	service := "Blog Posts API"
	log, err := logger.New(service)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer log.Sync()

	if err := run(log); err != nil {
		log.Errorw("start", "ERROR", err)
		log.Sync()
		os.Exit(1)
	}
}

func run(log *zap.SugaredLogger) error {
	log.Infow("start", "status", "memory cache")
	// TODO: cache expiry configuration
	c := cache.New(5*time.Minute, 10*time.Minute)

	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown, syscall.SIGINT, syscall.SIGTERM)
	srvCfg := api.Config{
		Log:   log,
		Cache: c,
	}
	srv := api.NewAPIServer(&srvCfg)

	serverErrors := make(chan error, 1)
	go func() {
		log.Infow("start", "status", "post-api started", "host", "http://localhost"+srv.Addr)
		serverErrors <- srv.ListenAndServe()
	}()

	select {
	case err := <-serverErrors:
		return fmt.Errorf("server error: %w", err)
	case sig := <-shutdown:
		log.Infow("shutdown", "status", "shutdown started", "signal", sig)
		defer log.Infow("shutdown", "status", "shutdown complete", "signal", sig)

		ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
		defer cancel()

		if err := srv.Shutdown(ctx); err != nil {
			srv.Close()
			return fmt.Errorf("failed to stop server gracefully: %w", err)
		}
	}
	return nil
}
